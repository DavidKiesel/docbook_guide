<?xml version="1.0" encoding="UTF-8"?>
<section
    xml:id="Operating_System_Inlines"
    xmlns="http://docbook.org/ns/docbook"
    version="5.0"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xi="http://www.w3.org/2001/XInclude"
>
    <title>Operating System Inlines</title>

    <table>
        <title>Operating System Inlines Summary</title>

        <tgroup cols="3">
            <thead>
                <row>
                    <entry>Element</entry>

                    <entry>Usage</entry>

                    <entry>Examples</entry>
                </row>
            </thead>

            <tbody>
                <row>
                    <entry>
                        <link linkend="command"><tag class="element">command</tag></link>
                    </entry>

                    <entry>
                        <para>
                            Name of an executable program or other software
                            command.  See also, <!-- TODO <link
                            linkend="application"> --><tag
                            class="element">application</tag><!-- </link> -->,
                            used to describe a software product.  E.g.,
                            <application>Firefox</application> is an
                            application; <command>firefox</command> is a
                            command.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <command>ls</command>
                        </para>

                        <para>
                            <command><replaceable>some_command</replaceable></command>
                        </para>
                    </entry>
                </row>

                <row>
                    <entry>
                        <link linkend="computeroutput"><tag class="element">computeroutput</tag></link>
                    </entry>

                    <entry>
                        <para>
                            Data displayed by a computer.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <computeroutput>Hello, world.</computeroutput>
                        </para>
                    </entry>
                </row>

                <row>
                    <entry>
                        <link linkend="envar"><tag class="element">envar</tag></link>
                    </entry>

                    <entry>
                        <para>
                            Environment variable.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <envar>PATH</envar>
                        </para>
                    </entry>
                </row>

                <row>
                    <entry>
                        <link linkend="filename"><tag class="element">filename</tag></link>
                    </entry>

                    <entry>
                        <para>
                            Name of a file.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <filename>passwd</filename>
                        </para>

                        <para>
                            <filename>/etc/passwd</filename>
                        </para>

                        <para>
                            <filename class="directory">/usr/bin</filename>
                        </para>

                        <para>
                            <filename><replaceable>SOME_FILE</replaceable></filename>
                        </para>
                    </entry>
                </row>

                <row>
                    <entry>
                        <link linkend="prompt"><tag class="element">prompt</tag></link>
                    </entry>

                    <entry>
                        <para>
                            Software prompt for input.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <prompt>$</prompt>
                        </para>

                        <para>
                            <prompt>#</prompt>
                        </para>

                        <para>
                            <prompt><![CDATA[>>>]]></prompt>
                        </para>

                        <para>
                            <prompt>Password? </prompt>
                        </para>
                    </entry>
                </row>

                <row>
                    <entry>
                        <link linkend="userinput"><tag class="element">userinput</tag></link>
                    </entry>

                    <entry>
                        <para>
                            User input.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <userinput>y</userinput>
                        </para>

                        <para>
                            <userinput>yes</userinput>
                        </para>

                        <para>
                            <userinput><replaceable>password</replaceable></userinput>
                        </para>
                    </entry>
                </row>
           </tbody>
        </tgroup>
    </table>

    <section
        xml:id="command"
    >
        <title><tag class="element">command</tag></title>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<command>ls</command>]]></programlisting>

            <para>
                <command>ls</command>
            </para>
        </informalexample>

        <informalexample>
            <programlisting language="xml"><![CDATA[<command><replaceable>some_command</replaceable></command>]]></programlisting>

            <para>
                <command><replaceable>some_command</replaceable></command>
            </para>
        </informalexample>
    </section>

    <section
        xml:id="computeroutput"
    >
        <title><tag class="element">computeroutput</tag></title>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<computeroutput>Hello, world.</computeroutput>]]></programlisting>

            <para>
                <computeroutput>Hello, world.</computeroutput>
            </para>
        </informalexample>
    </section>

    <section
        xml:id="envar"
    >
        <title><tag class="element">envar</tag></title>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<envar>PATH</envar>]]></programlisting>

            <para>
                <envar>PATH</envar>
            </para>
        </informalexample>
    </section>

    <section
        xml:id="filename"
    >
        <title><tag class="element">filename</tag></title>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<filename>passwd</filename>]]></programlisting>

            <para>
                <filename>passwd</filename>
            </para>
        </informalexample>
        
        <informalexample>
            <programlisting language="xml"><![CDATA[<filename>/etc/passwd</filename>]]></programlisting>

            <para>
                <filename>/etc/passwd</filename>
            </para>
        </informalexample>
        
        <informalexample>
            <programlisting language="xml"><![CDATA[<filename class="directory">/usr/bin</filename>]]></programlisting>

            <para>
                <filename class="directory">/usr/bin</filename>
            </para>
        </informalexample>
        
        <informalexample>
            <programlisting language="xml"><![CDATA[<filename><replaceable>SOME_FILE</replaceable></filename>]]></programlisting>

            <para>
                <filename><replaceable>SOME_FILE</replaceable></filename>
            </para>
        </informalexample>
    </section>

    <section
        xml:id="prompt"
    >
        <title><tag class="element">prompt</tag></title>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<prompt>$</prompt>]]></programlisting>

            <para>
                <prompt>$</prompt>
            </para>
        </informalexample>
        
        <informalexample>
            <programlisting language="xml"><![CDATA[<prompt>#</prompt>]]></programlisting>

            <para>
                <prompt>#</prompt>
            </para>
        </informalexample>
        
        <informalexample>
            <programlisting language="xml"><![CDATA[<prompt><![CDATA[>>>]]>&#93;&#93;&#62;<![CDATA[</prompt>]]></programlisting>

            <para>
                <prompt><![CDATA[>>>]]></prompt>
            </para>
        </informalexample>
        
        <informalexample>
            <programlisting language="xml"><![CDATA[<prompt>Password? </prompt>]]></programlisting>

            <para>
                <prompt>Password? </prompt>
            </para>
        </informalexample>
    </section>

    <section
        xml:id="userinput"
    >
        <title><tag class="element">userinput</tag></title>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<userinput>y</userinput>]]></programlisting>

            <para>
                <userinput>y</userinput>
            </para>
        </informalexample>
        
        <informalexample>
            <programlisting language="xml"><![CDATA[<userinput>yes</userinput>]]></programlisting>

            <para>
                <userinput>yes</userinput>
            </para>
        </informalexample>
        
        <informalexample>
            <programlisting language="xml"><![CDATA[<userinput><replaceable>password</replaceable></userinput>]]></programlisting>

            <para>
                <userinput><replaceable>password</replaceable></userinput>
            </para>
        </informalexample>
    </section>
</section>
