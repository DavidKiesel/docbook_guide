<?xml version="1.0" encoding="UTF-8"?>
<section
    xml:id="Keyboard_Inlines"
    xmlns="http://docbook.org/ns/docbook"
    version="5.0"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xmlns:xi="http://www.w3.org/2001/XInclude"
>
    <title>Keyboard Inlines</title>

    <table>
        <title>Keyboard Inlines Summary</title>

        <tgroup cols="3">
            <thead>
                <row>
                    <entry>Element</entry>

                    <entry>Usage</entry>

                    <entry>Examples</entry>
                </row>
            </thead>

            <tbody>
                <row>
                    <entry>
                        <link linkend="accel"><tag class="element">accel</tag></link>
                    </entry>

                    <entry>
                        <para>
                            Accelerator; <acronym>GUI</acronym> keyboard shortcut.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <!-- accel element does not validate within a para element -->
                            <!-- E<accel function="home">x</accel>it -->
                            <guimenuitem>E<accel>x</accel>it</guimenuitem>
                        </para>
                    </entry>
                </row>

                <row>
                    <entry>
                        <link linkend="keycap"><tag class="element">keycap</tag></link>
                    </entry>

                    <entry>
                        <para>
                            Text on a key on a keyboard.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <keycap function="home">Home</keycap>
                        </para>
                    </entry>
                </row>

                <row>
                    <entry>
                        <link linkend="keycode"><tag class="element">keycode</tag></link>
                    </entry>

                    <entry>
                        <para>
                            <link
                            xlink:href="https://en.wikipedia.org/wiki/Scancode">Scancode</link>
                            generated by a key on a keyboard.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <keycode>0x61</keycode>
                        </para>
                    </entry>
                </row>

                <row>
                    <entry>
                        <link linkend="keycombo"><tag class="element">keycombo</tag></link>
                    </entry>

                    <entry>
                        <para>
                            Combination of input actions.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <keycombo>
                                <keycap function="alt">Alt</keycap>
                                <keycap function="control">Ctrl</keycap>
                                <keycap function="delete">Delete</keycap>
                            </keycombo>
                        </para>
                    </entry>
                </row>

                <row>
                    <entry>
                        <link linkend="keysym"><tag class="element">keysym</tag></link>
                    </entry>

                    <entry>
                        <para>
                            Symbolic name of a key on a keyboard.  May or may
                            not differ from the <link
                            linkend="keycap">keycap</link>.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <keysym>Home</keysym>
                        </para>
                    </entry>
                </row>

                <row>
                    <entry>
                        <link linkend="shortcut"><tag class="element">shortcut</tag></link>
                    </entry>

                    <entry>
                        <para>
                            Key combination for an action that is also
                            accessible through a menu.
                        </para>
                    </entry>

                    <entry>
                        <para>
                            <shortcut>
                                <keycombo>
                                    <keycap function="alt">Alt</keycap>
                                    <keycap function="control">Ctrl</keycap>
                                    <keycap function="delete">Delete</keycap>
                                </keycombo>
                            </shortcut>
                        </para>
                    </entry>
                </row>
           </tbody>
        </tgroup>
    </table>

   <section
        xml:id="accel"
    >
        <title><tag class="element">accel</tag></title>

        <para>
            Note that <tag class="element">accel</tag> as a child of <tag
            class="element">para</tag> will build under
            <package>docbkx-maven-pluginwork</package>.  But it will fail
            validation through <command>xmllint</command>.
        </para>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<guimenuitem>E<accel>x</accel>it</guimenuitem>]]></programlisting>

            <para>
                <guimenuitem>E<accel>x</accel>it</guimenuitem>
            </para>
        </informalexample>
    </section>

    <section
        xml:id="keycap"
    >
        <title><tag class="element">keycap</tag></title>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<keycap function="home">Home</keycap>]]></programlisting>

            <para>
                <keycap function="home">Home</keycap>
            </para>
        </informalexample>
    </section>

    <section
        xml:id="keycode"
    >
        <title><tag class="element">keycode</tag></title>

        <para>
            <link xlink:href="http://www.win.tue.nl/~aeb/linux/kbd/scancodes.html"/>
        </para>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<keycode>0x61</keycode>]]></programlisting>

            <para>
                <keycode>0x61</keycode>
            </para>
        </informalexample>
    </section>

    <section
        xml:id="keycombo"
    >
        <title><tag class="element">keycombo</tag></title>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<keycombo>
    <keycap function="alt">Alt</keycap>
    <keycap function="control">Ctrl</keycap>
    <keycap function="delete">Delete</keycap>
</keycombo>]]></programlisting>


            <para>
                <keycombo>
                    <keycap function="alt">Alt</keycap>
                    <keycap function="control">Ctrl</keycap>
                    <keycap function="delete">Delete</keycap>
                </keycombo>
            </para>
        </informalexample>
    </section>

    <section
        xml:id="keysym"
    >
        <title><tag class="element">keysym</tag></title>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<keysym>Home</keysym>]]></programlisting>

            <para>
                <keysym>Home</keysym>
            </para>
        </informalexample>
    </section>

    <section
        xml:id="shortcut"
    >
        <title><tag class="element">shortcut</tag></title>

        <para>
            Example(s):
        </para>

        <informalexample>
            <programlisting language="xml"><![CDATA[<shortcut>
    <keycombo>
        <keycap function="alt">Alt</keycap>
        <keycap function="control">Ctrl</keycap>
        <keycap function="delete">Delete</keycap>
    </keycombo>
</shortcut>]]></programlisting>

            <para>
                <shortcut>
                    <keycombo>
                        <keycap function="alt">Alt</keycap>
                        <keycap function="control">Ctrl</keycap>
                        <keycap function="delete">Delete</keycap>
                    </keycombo>
                </shortcut>
            </para>
        </informalexample>
    </section>
</section>
